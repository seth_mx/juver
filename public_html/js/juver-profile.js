$(function () {

    $('#discover').on('click', function () {
        $('.container').loading({
            stoppable: true,
            message: 'Analizing...'
        });

        $.ajax({
            url: "/data.json",
            context: document.body,
            dataType: "json",
            timeout: 100000,
        }).done(function (response) {
            $('.container').loading('stop');

            var data = [];
            var dataLabels = [];

            if (response.personalities) {

                var config = {
                    type: 'doughnut',
                    data: {
                        datasets: [{
                                data: data,
                                backgroundColor: [
                                    "#4dc9f6", "#f67019", "#f53794", "#537bc4", "#acc236"
                                ],
                                label: 'Dataset 1'
                            }],
                        labels: dataLabels
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Personality insights'
                        },
                        animation: {
                            animateScale: true,
                            animateRotate: true
                        }
                    }
                };

                $.each(response.personalities, function (k, personality) {
                    data[k] = parseFloat(personality.percentage).toFixed(2);
                    dataLabels[k] = personality.name;
                });

                $.each(response.places, function (k, place) {

                    var insight = Math.floor(Math.random() * (5 - 1 + 1)) + 1;

                    var cardItem = '<div class="col-xs-12 col-lg-6 col-md-12">';
                    cardItem += '<div class="card card-cascade wider reverse my-4">';
                    cardItem += '<div class="view overlay hm-white-slight">';
                    cardItem += '<img src="' + place.photo + '" class="img-fluid" />';
                    cardItem += '<a href="#!">';
                    cardItem += '<div class="mask"></div>';
                    cardItem += '</a>';
                    cardItem += '</div>';
                    cardItem += '<div class="personality-insight-' + insight + '">&nbsp;</div>';
                    cardItem += '<div class="card-body text-center">';
                    cardItem += '<h4 class="card-title place-title" >' + place.name + '</h4>';
                    cardItem += '<h3 class="place-category"><i class="fa fa-map-marker"> </i> ' + place.address + '</h3>';
                    cardItem += '<div class="place-info">';
                    cardItem += '<div class="place-category"><span><i class="fa fa-star"> </i>Rating: </span> ' + parseFloat(place.rating).toFixed(2) + '</div>';
                    cardItem += '<a class="btn btn-md btn-blue btn-label-map show-map" href="https://www.google.com/maps/search/?api=1&query=' + place.latitude + ',' + place.longitude + '" style="margin-bottom: 15px;">';
                    cardItem += '<span class=""><i class="fa fa-map-o"></i></span> Show map';
                    cardItem += '</a>';
                    cardItem += '</div>';
                    cardItem += '</div>';
                    cardItem += '</div>';
                    cardItem += '</div>';

                    $("#result-places").append(cardItem);
                });

                var ctx = document.getElementById("chart-personality").getContext("2d");
                new Chart(ctx, config);

                $("#juver-analysis-result").slideDown();
            }


        });
    });


});